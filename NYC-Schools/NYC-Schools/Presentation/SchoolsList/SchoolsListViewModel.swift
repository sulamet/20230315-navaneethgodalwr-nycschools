//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/


import Foundation

protocol SchoolsListViewModel {
    var pageTitle: String { get }
    var schoolsList: [NYCSchools] { get }
    func buildModel(
        onComplete: @escaping EmptySuccessBlock,
        loadingBlock: LoadingBlock
    )
}

final class SchoolsListViewModelImpl: SchoolsListViewModel {
    var pageTitle: String { "NYC Schools List" }
    var schoolsList: [NYCSchools] = []
    
    private let repo: NYCRepository
    init(repository: NYCRepository = NYCRepositoryImpl()) {
        self.repo = repository
    }
    
    func buildModel(
        onComplete: @escaping EmptySuccessBlock,
        loadingBlock: LoadingBlock
    ) {
        repo.getNYCSchoolList(
            onComplete: { result in
                switch result {
                case .success(let schoolsList):
                    self.schoolsList = schoolsList
                    onComplete(.success(()))
                case .failure(let error):
                    onComplete(.failure(error))
                }
            },
            loadingBlock: loadingBlock
        )
    }
}
