//
/* 
 Created by navneet
 See LICENSE folder for licensing information.
 */

typealias SchoolsSuccessBlock = (Result<[NYCSchools], Error>) -> Void
typealias SATScoresSuccessBlock = (Result<[SATScores], Error>) -> Void
typealias EmptySuccessBlock = (Result<Void, Error>) -> Void

protocol NYCRepository {
    func getNYCSchoolList(
        onComplete: @escaping SchoolsSuccessBlock,
        loadingBlock: LoadingBlock
    )
    
    func getSATScores(
        onComplete: @escaping SATScoresSuccessBlock,
        loadingBlock: LoadingBlock
    )
}
