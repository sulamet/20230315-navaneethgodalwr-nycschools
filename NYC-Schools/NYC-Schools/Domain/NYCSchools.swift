//
/* 
 Created by navneet
 See LICENSE folder for licensing information.
 */

import Foundation

struct NYCSchools: Decodable {
    let dbn: String
    let schoolName: String
    let boro: String?
    let overviewParagraph: String
    let school10ThSeats: String?
    let academicopportunities1: String?
    let academicopportunities2: String?
    let ellPrograms: String?
    let neighborhood: String?
    let buildingCode: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?
    let bus: String?
    let grades2018: String?
    let finalgrades: String?
    let totalStudents: String?
    let extracurricularActivities: String?
    let schoolSports: String?
    let attendanceRate: String?
    let pctStuEnoughVariety: String?
    let pctStuSafe: String?
    let schoolAccessibilityDescription: String?
    let directions1: String?
    let requirement11: String?
    let requirement21: String?
    let requirement31: String?
    let requirement41: String?
    let requirement51: String?
    let offerRate1: String?
    let program1: String?
    let code1: String?
    let interest1: String?
    let method1: String?
    let seats9Ge1: String?
    let grade9Gefilledflag1: String?
    let grade9Geapplicants1: String?
    let seats9Swd1: String?
    let grade9Swdfilledflag1: String?
    let grade9Swdapplicants1: String?
    let seats101: String?
    let admissionspriority11: String?
    let admissionspriority21: String?
    let admissionspriority31: String?
    let grade9Geapplicantsperseat1: String?
    let grade9Swdapplicantsperseat1: String?
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    let latitude: String?
    let longitude: String?
    let communityBoard: String?
    let councilDistrict: String?
    let censusTract: String?
    let bin: String?
    let bbl: String?
    let nta: String?
    let borough: String??
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case boro = "boro"
        case overviewParagraph = "overview_paragraph"
        case school10ThSeats = "school_10th_seats"
        case academicopportunities1 = "academicopportunities1"
        case academicopportunities2 = "academicopportunities2"
        case ellPrograms = "ell_programs"
        case neighborhood = "neighborhood"
        case buildingCode = "building_code"
        case location = "location"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website = "website"
        case subway = "subway"
        case bus = "bus"
        case grades2018 = "grades2018"
        case finalgrades = "finalgrades"
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case pctStuSafe = "pct_stu_safe"
        case schoolAccessibilityDescription = "school_accessibility_description"
        case directions1 = "directions1"
        case requirement11 = "requirement1_1"
        case requirement21 = "requirement2_1"
        case requirement31 = "requirement3_1"
        case requirement41 = "requirement4_1"
        case requirement51 = "requirement5_1"
        case offerRate1 = "offer_rate1"
        case program1 = "program1"
        case code1 = "code1"
        case interest1 = "interest1"
        case method1 = "method1"
        case seats9Ge1 = "seats9ge1"
        case grade9Gefilledflag1 = "grade9gefilledflag1"
        case grade9Geapplicants1 = "grade9geapplicants1"
        case seats9Swd1 = "seats9swd1"
        case grade9Swdfilledflag1 = "grade9swdfilledflag1"
        case grade9Swdapplicants1 = "grade9swdapplicants1"
        case seats101 = "seats101"
        case admissionspriority11 = "admissionspriority11"
        case admissionspriority21 = "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case grade9Geapplicantsperseat1 = "grade9geapplicantsperseat1"
        case grade9Swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
        case primaryAddressLine1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case stateCode = "state_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case censusTract = "census_tract"
        case bin = "bin"
        case bbl = "bbl"
        case nta = "nta"
        case borough = "borough"
    }
}
