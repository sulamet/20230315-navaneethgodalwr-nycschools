//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/


import UIKit

extension UIStackView {
    func addSpacer(_ height: CGFloat = 15) {
        let uiview = UIView(frame: .zero)
        addArrangedSubview(uiview)
        uiview.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    @discardableResult
    func addLabel(
        _ title: String,
        _ color: UIColor = .black,
        alignment: NSTextAlignment = .center
    ) -> UILabel {
        let label = UILabel()
        label.text = title
        label.numberOfLines = 0
        label.textAlignment = alignment
        label.textColor = color
        addArrangedSubview(label)
        return label
    }
    
    func clear() {
        arrangedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
